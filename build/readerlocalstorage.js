(function() {
  var FavoriteService, FeatureService, PaintService, PinService, ReaderLocalStorage,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  FeatureService = (function() {
    function FeatureService() {}

    FeatureService.prototype.localStorage = function() {
      return localStorage.getItem(this.collectionName + "_" + RDP.options.publicationId);
    };

    FeatureService.prototype.setlocalStorage = function(data) {
      return localStorage.setItem(this.collectionName + "_" + RDP.options.publicationId, JSON.stringify(data));
    };

    FeatureService.prototype.collection = function() {
      var local;
      if (local = this.localStorage()) {
        return JSON.parse(local);
      } else {
        return [];
      }
    };

    FeatureService.prototype.replaceCollection = function(newValue) {
      return this.setlocalStorage(newValue);
    };

    FeatureService.prototype.publicationThumbURL = function() {
      return RDP.options.sunflower_address + "/html/getCoverToEdition?publicationId=" + RDP.options.publicationId + "&editionId=" + RDP.edition.id + "&width=&height=&quality=100";
    };

    FeatureService.prototype.addToCollection = function(item) {
      var collection;
      collection = this.collection();
      collection.push(item);
      this.replaceCollection(collection);
      return item;
    };

    FeatureService.prototype.removeFromCollection = function(itemToRemove) {
      var collection, i, item, j, len;
      collection = this.collection();
      for (i = j = 0, len = collection.length; j < len; i = ++j) {
        item = collection[i];
        if (item.id === itemToRemove.id) {
          collection.splice(i, 1);
          break;
        }
      }
      this.replaceCollection(collection);
      return itemToRemove;
    };

    FeatureService.prototype.updateItemCollection = function(id, newItem) {
      var collection, j, len, local;
      collection = this.collection();
      for (j = 0, len = collection.length; j < len; j++) {
        local = collection[j];
        if (id === local.id) {
          $.extend(local, newItem);
        }
      }
      this.replaceCollection(collection);
      return newItem;
    };

    return FeatureService;

  })();

  (typeof exports !== "undefined" && exports !== null ? exports : window).FeatureService = FeatureService;

  FavoriteService = (function(superClass) {
    extend(FavoriteService, superClass);

    function FavoriteService() {
      return FavoriteService.__super__.constructor.apply(this, arguments);
    }

    FavoriteService.prototype.collectionName = "favorites";

    FavoriteService.prototype.updateReader = function(data) {
      return RDP.reader.faves.updateList($.map(data, function(pageSet) {
        var page;
        page = typeof pageSet === "object" ? pageSet : RDP.edition.getPageSetById(pageSet);
        return {
          page: page,
          pageId: page.id,
          pageLabel: page.pageLabel,
          sectionIndex: page.sectionIndex
        };
      }));
    };

    FavoriteService.prototype.map = function(value) {
      return value.page;
    };

    FavoriteService.prototype.create = function(pageSet) {
      var favorite;
      favorite = {
        id: pageSet.id,
        publication_id: RDP.options.publicationId,
        thumb_url: (pageSet.portrait || pageSet.landscape).thumb,
        page: pageSet.id,
        publication: {
          title: RDP.edition.name,
          thumb_url: this.publicationThumbURL()
        }
      };
      return Promise.resolve(this.addToCollection(favorite).id);
    };

    FavoriteService.prototype.destroy = function(pageSet) {
      return Promise.resolve(this.removeFromCollection(pageSet).id);
    };

    return FavoriteService;

  })(FeatureService);

  (typeof exports !== "undefined" && exports !== null ? exports : window).FavoriteService = FavoriteService;

  PinService = (function(superClass) {
    extend(PinService, superClass);

    function PinService() {
      return PinService.__super__.constructor.apply(this, arguments);
    }

    PinService.prototype.collectionName = "pins";

    PinService.prototype.map = function(value) {
      return {
        pageId: value.page,
        text: value.body,
        coords: [parseFloat(value.x) * 100, parseFloat(value.y) * 100],
        id: value.id,
        title: ""
      };
    };

    PinService.prototype.updateReader = function(data) {
      var key, name, pages, records, value;
      pages = {};
      for (key in data) {
        value = data[key];
        (pages[name = value.pageId] || (pages[name] = [])).push(value);
      }
      records = $.map(pages, function(notes, pageId) {
        return {
          notes: notes,
          pageId: pageId
        };
      });
      return RDP.reader.notes.add({
        list: records
      });
    };

    PinService.prototype.create = function(note) {
      var pin;
      pin = {
        body: note.text,
        x: note.coords[0] / 100.0,
        y: note.coords[1] / 100.0,
        page: note.pageId,
        id: note.id,
        publication_id: RDP.options.publicationId,
        thumb_url: note.thumb,
        publication: {
          title: RDP.edition.name,
          thumb_url: this.publicationThumbURL()
        }
      };
      return Promise.resolve(this.addToCollection(pin));
    };

    PinService.prototype.destroy = function(note) {
      return Promise.resolve(this.removeFromCollection(note));
    };

    PinService.prototype.update = function(note) {
      var newPin;
      newPin = {
        body: note.text,
        x: note.coords[0] / 100.0,
        y: note.coords[1] / 100.0,
        page: note.pageId
      };
      return Promise.resolve(this.updateItemCollection(note.id, newPin));
    };

    return PinService;

  })(FeatureService);

  (typeof exports !== "undefined" && exports !== null ? exports : window).PinService = PinService;

  PaintService = (function(superClass) {
    extend(PaintService, superClass);

    function PaintService() {
      return PaintService.__super__.constructor.apply(this, arguments);
    }

    PaintService.prototype.collectionName = "paints";

    PaintService.prototype.map = function(value) {
      return value;
    };

    PaintService.prototype.updateReader = function(data) {
      var key, value;
      return RDP.reader.paint.add((function() {
        var results;
        results = [];
        for (key in data) {
          if (!hasProp.call(data, key)) continue;
          value = data[key];
          results.push(value);
        }
        return results;
      })());
    };

    PaintService.prototype.create = function(paint) {
      paint.publication_id = RDP.options.publicationId;
      return Promise.resolve(this.addToCollection(paint));
    };

    PaintService.prototype.destroy = function(paint) {
      return Promise.resolve(this.removeFromCollection(paint).id);
    };

    PaintService.prototype.update = function(newPaint) {
      return Promise.resolve(this.updateItemCollection(newPaint.id, newPaint));
    };

    return PaintService;

  })(FeatureService);

  (typeof exports !== "undefined" && exports !== null ? exports : window).PaintService = PaintService;

  ReaderLocalStorage = (function() {
    function ReaderLocalStorage(reader) {
      this.reader = reader;
      this.favorites = new FavoriteService();
      this.pins = new PinService();
      this.paints = new PaintService();
      this.reader.on("load", (function(_this) {
        return function() {
          _this.bindEvents();
          return _this.load();
        };
      })(this));
    }

    ReaderLocalStorage.prototype.bindEvents = function() {
      this.reader.faves.on('add', (function(_this) {
        return function(record) {
          return _this.persist(record, 'favorites', 'create');
        };
      })(this));
      this.reader.faves.on('remove', (function(_this) {
        return function(record) {
          return _this.persist(record, 'favorites', 'destroy');
        };
      })(this));
      this.reader.faves.on("add, remove", (function(_this) {
        return function() {
          return _this.fromLocal(_this.favorites);
        };
      })(this));
      this.reader.notes.on('create', (function(_this) {
        return function(record) {
          return _this.persist(record, 'pins', 'create');
        };
      })(this));
      this.reader.notes.on('destroy', (function(_this) {
        return function(record) {
          return _this.persist(record, 'pins', 'destroy');
        };
      })(this));
      this.reader.notes.on('update', (function(_this) {
        return function(record) {
          return _this.persist(record, 'pins', 'update');
        };
      })(this));
      this.reader.notes.on("create, update, destroy", (function(_this) {
        return function() {
          return _this.fromLocal(_this.pins);
        };
      })(this));
      this.reader.paint.on('create', (function(_this) {
        return function(record) {
          return _this.persist(record, 'paints', 'create');
        };
      })(this));
      this.reader.paint.on('destroy', (function(_this) {
        return function(record) {
          return _this.persist(record, 'paints', 'destroy');
        };
      })(this));
      this.reader.paint.on('update', (function(_this) {
        return function(record) {
          return _this.persist(record, 'paints', 'update');
        };
      })(this));
      return this.reader.paint.on("create, update, destroy", (function(_this) {
        return function() {
          return _this.fromLocal(_this.paints);
        };
      })(this));
    };

    ReaderLocalStorage.prototype.getUniqueValues = function(collectionName, data, map) {
      var unique;
      unique = {};
      data.forEach(function(value) {
        return unique[collectionName + "-" + value.id] = map(value);
      });
      return unique;
    };

    ReaderLocalStorage.prototype.fromLocal = function(feature) {
      var data;
      data = $.map(feature.collection(), function(item) {
        if (item.publication_id === RDP.options.publicationId) {
          return item;
        }
      });
      return feature.updateReader(this.getUniqueValues(feature.collectionName, data, feature.map));
    };

    ReaderLocalStorage.prototype.load = function() {
      if (RDP.options.load.paints) {
        this.reader.paint.load(RDP.edition.id);
      }
      if (RDP.options.load.highlights) {
        this.reader.highlights.load(RDP.edition.id);
      }
      if (RDP.options.load.print) {
        this.reader.print.load(RDP.edition.id);
      }
      if (RDP.options.load.notes) {
        this.reader.notes.load(RDP.edition.id);
      }
      if (RDP.options.load.faves) {
        this.reader.faves.load(RDP.edition.id);
      }
      if (RDP.reader) {
        this.fromLocal(this.favorites);
        this.fromLocal(this.pins);
        return this.fromLocal(this.paints);
      }
    };

    ReaderLocalStorage.prototype.persist = function(record, collection, method) {
      return this[collection][method](record);
    };

    return ReaderLocalStorage;

  })();

  (typeof exports !== "undefined" && exports !== null ? exports : window).ReaderLocalStorage = ReaderLocalStorage;

}).call(this);
