<a name"1.3.0"></a>
## 1.3.0 (2016-05-05)


#### Bug Fixes

* **Favorites:** check for thumb_url on portrait or landscape pageset (0356eb2f)


<a name"1.2.0"></a>
## 1.2.0 (2016-02-17)


#### Features

* store and load favorite page if offline (90cdb8d9)


<a name"1.1.0"></a>
## 1.1.0 (2015-11-19)


#### Features

* expose Services to be used for others projects (03353afa)


<a name="1.0.0"></a>
## 1.0.0 (2015-03-23)

