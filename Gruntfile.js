module.exports = function(grunt) {
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
    '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
    '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
    '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
    ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',

    coffee: {
      src: {
        expand: true,
        flatten: true,
        cwd: 'src',
        src: ['*.coffee'],
        dest: 'build',
        ext: '.js'
      }
    },
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      dist: {
        src: '<%= coffee.src.dest %>/<%= pkg.name %>.js',
        dest: '<%= coffee.src.dest %>/<%= pkg.name %>.min.js'
      }
    },
    build: {
      tasks: ['buildTask', 'changelog', 'git:add:--all', 'git:commit:bumped version', 'git:tag:bumped version']
    },
    gitinfo: {},
    aws: grunt.file.readJSON("credentials.json"),
    s3: {
      options: {
        accessKeyId: "<%= aws.accessKeyId %>",
        secretAccessKey: "<%= aws.secretAccessKey %>",
        bucket: 'cdn-html5',
        httpOptions: {
          proxy: process.env.http_proxy
        },
        sslEnabled: false
      },
      dist: {
        files: [
          {
            cwd: 'build',
            src: ["<%= pkg.name %>*"],
            dest: "rdpreader_localstorage/<%= pkg.version %>/"
          }
        ]
      },
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-conventional-changelog');
  grunt.loadNpmTasks('grunt-bump-build-git');
  grunt.loadNpmTasks('grunt-gitinfo');
  grunt.loadNpmTasks('grunt-aws');


  grunt.registerTask('buildTask', ['coffee', 'uglify']);
  grunt.registerTask('deploy', ['gitinfo', 's3']);
  grunt.registerTask('default', ['buildTask']);
};