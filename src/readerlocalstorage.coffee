class FeatureService
  localStorage: ->
    localStorage.getItem("#{@collectionName}_#{RDP.options.publicationId}")

  setlocalStorage: (data) ->
    localStorage.setItem("#{@collectionName}_#{RDP.options.publicationId}", JSON.stringify(data))

  collection: ->  if local = @localStorage() then JSON.parse(local) else []

  replaceCollection: (newValue)-> @setlocalStorage(newValue)

  publicationThumbURL: ->
    "#{RDP.options.sunflower_address}/html/getCoverToEdition?publicationId=#{RDP.options.publicationId}&editionId=#{RDP.edition.id}&width=&height=&quality=100"

  addToCollection: (item)->
    collection = @collection()
    collection.push(item)
    @replaceCollection(collection)
    item

  removeFromCollection: (itemToRemove)->
    collection = @collection()
    for item, i in collection
      if item.id is itemToRemove.id
        collection.splice(i, 1)
        break
    @replaceCollection(collection)
    itemToRemove

  updateItemCollection: (id, newItem) ->
    collection = @collection()
    for local in collection
      $.extend(local, newItem) if id is local.id
    @replaceCollection(collection)
    newItem

(exports ? window).FeatureService = FeatureService

class FavoriteService extends FeatureService
  collectionName: "favorites"

  updateReader: (data)->
    RDP.reader.faves.updateList $.map data, (pageSet) ->
      page = if typeof pageSet == "object" then pageSet else RDP.edition.getPageSetById pageSet
      page: page
      pageId: page.id
      pageLabel: page.pageLabel
      sectionIndex: page.sectionIndex

  map: (value) -> value.page

  create: (pageSet)->
    favorite =
      id: pageSet.id
      publication_id: RDP.options.publicationId
      thumb_url: (pageSet.portrait || pageSet.landscape).thumb
      page: pageSet.id
      publication:
        title: RDP.edition.name
        thumb_url: @publicationThumbURL()

    Promise.resolve @addToCollection(favorite).id

  destroy:  (pageSet)-> Promise.resolve @removeFromCollection(pageSet).id

(exports ? window).FavoriteService = FavoriteService

class PinService extends FeatureService
  collectionName: "pins"

  map: (value)->
    pageId: value.page
    text: value.body
    coords: [parseFloat(value.x) * 100, parseFloat(value.y) * 100]
    id: value.id
    title: ""

  updateReader: (data)->
    pages = {}
    for key, value of data
      (pages[value.pageId] ||= []).push value
    records = $.map pages, (notes, pageId) ->
      notes: notes
      pageId: pageId
    RDP.reader.notes.add list: records

  create: (note) ->
    pin =
      body: note.text
      x: note.coords[0] / 100.0
      y: note.coords[1] / 100.0
      page: note.pageId
      id: note.id
      publication_id: RDP.options.publicationId
      thumb_url: note.thumb
      publication:
        title: RDP.edition.name
        thumb_url: @publicationThumbURL()

    Promise.resolve @addToCollection(pin)

  destroy: (note)-> Promise.resolve @removeFromCollection(note)

  update: (note)->
    newPin =
      body: note.text
      x: note.coords[0] / 100.0
      y: note.coords[1] / 100.0
      page: note.pageId

    Promise.resolve @updateItemCollection(note.id, newPin)

(exports ? window).PinService = PinService

class PaintService extends FeatureService
  collectionName: "paints"

  map: (value) -> value

  updateReader: (data)-> RDP.reader.paint.add(value for own key, value of data)

  create: (paint)->
    paint.publication_id = RDP.options.publicationId
    Promise.resolve @addToCollection(paint)

  destroy:  (paint)-> Promise.resolve @removeFromCollection(paint).id

  update: (newPaint)-> Promise.resolve @updateItemCollection(newPaint.id, newPaint)

(exports ? window).PaintService = PaintService

class ReaderLocalStorage
  constructor: (@reader)->
    @favorites = new FavoriteService()
    @pins = new PinService()
    @paints = new PaintService()

    @reader.on "load", =>
      @bindEvents()
      @load()

  bindEvents: ->
    @reader.faves.on 'add', (record) => @persist record, 'favorites', 'create'
    @reader.faves.on 'remove', (record) => @persist record, 'favorites', 'destroy'
    @reader.faves.on "add, remove", => @fromLocal(@favorites)

    @reader.notes.on 'create', (record) => @persist record, 'pins', 'create'
    @reader.notes.on 'destroy', (record) => @persist record, 'pins', 'destroy'
    @reader.notes.on 'update', (record) => @persist record, 'pins', 'update'
    @reader.notes.on "create, update, destroy", => @fromLocal(@pins)

    @reader.paint.on 'create', (record) => @persist record, 'paints', 'create'
    @reader.paint.on 'destroy', (record) => @persist record, 'paints', 'destroy'
    @reader.paint.on 'update', (record) => @persist record, 'paints', 'update'
    @reader.paint.on "create, update, destroy", => @fromLocal(@paints)

  getUniqueValues: (collectionName, data, map) ->
    unique = {}
    data.forEach (value) ->
      unique["#{collectionName}-#{value.id}"] = map value
    unique

  fromLocal: (feature)->
    data = $.map feature.collection(), (item) -> item if item.publication_id is RDP.options.publicationId
    feature.updateReader @getUniqueValues(feature.collectionName, data, feature.map)

  load: ->
    @reader.paint.load(RDP.edition.id) if RDP.options.load.paints
    @reader.highlights.load(RDP.edition.id) if RDP.options.load.highlights
    @reader.print.load(RDP.edition.id) if RDP.options.load.print
    @reader.notes.load(RDP.edition.id) if RDP.options.load.notes
    @reader.faves.load(RDP.edition.id) if RDP.options.load.faves

    if RDP.reader
      @fromLocal(@favorites)
      @fromLocal(@pins)
      @fromLocal(@paints)

  persist: (record, collection, method) -> @[collection][method](record)

(exports ? window).ReaderLocalStorage = ReaderLocalStorage
